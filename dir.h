#ifndef DIR_H
#define DIR_H
#include <dirent.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "tools.h"

#define LIST_DIRS 0
#define LIST_FILES 1

#define SORT_MTIME 2
#define SORT_CTIME 4
#define SORT_NAME 8 
#define SORT_TITLE 16 //unimplemented
#define SORT_REVERSE 1 //OR/XOR with the others to reverse 

typedef struct {
	char *name;
	char path[1024];
 	int ctime;
	int mtime;	
} dir_file;

dir_file *list_items(char *dir_path, bool type);
dir_file *sort_dir(dir_file *files, int sort_type);
#endif
