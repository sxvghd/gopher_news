#include "tools.h"
#include "dir.h"

s_date get_sdate(int timestamp)
{
    time_t convtime = (time_t)timestamp;
    struct tm *conv_date = gmtime(&convtime);
    s_date date;
    date.day = conv_date->tm_mday;
    date.month = conv_date->tm_mon+1;
    date.year = conv_date->tm_year+1900;
    return date;
}

char *get_article_title(char *path)
{
    FILE *file = fopen(path,"r");
    int length = 1;
    //get length of the first line
    while(fgetc(file) != '\n')
        length++;
    rewind(file);
    char *line = calloc(length,sizeof(char));
    fgets(line, length, file);
    //Assuming that the title will use only one #
    return line+1;
}

void update_selectors()
{
	struct stat stbuf;
	if(stat("Categories/.gopher.rec",&stbuf) != -1)
	{
    	time_t convtime = stbuf.st_mtime;
		struct tm *file_time = gmtime(&convtime);
		int hour = file_time->tm_hour;
    	convtime = time(NULL);
		struct tm *now_time = gmtime(&convtime);
		if(hour == now_time->tm_hour)
			return;
	}
	FILE *config = fopen("Categories/.gopher.rec","w");
	dir_file *dirs = list_items("Categories",LIST_DIRS);
	if(dirs == NULL || dirs[0].ctime == 0)
    {
        puts(".3No directories found!   /\n");
		if(dirs == NULL)
        	free(dirs);
        return;
    }
	int count = 0;
	while(dirs[count].ctime != 0)
    {
        fprintf(config,"!dirproc %s /var/gopher/whole/front\n",dirs[count].name);
		count++;
    }
	fclose(config);
	free(dirs);
	return;
}
