#ifndef TOOLS_H
#define TOOLS_H
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <time.h>

typedef struct {
    int day;
    int month;
    int year;
} s_date;

s_date get_sdate(int timestamp);
char *get_article_title(char *path);
void update_selectors();
#endif
