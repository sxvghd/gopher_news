#include <stdio.h>
#include "dir.h"

#define swap(a,b,t) {\
		dir_file temp = b;\
		b = a;\
		a = temp;\
		t = true;\
		}

dir_file *list_items(char *dir_path, bool type)
{
	dir_file *files = calloc(0,sizeof(dir_file));
	int count = 0;
	DIR *directory = opendir(dir_path);
	if(directory == NULL)
		return NULL;
	struct dirent *article_file = readdir(directory);
	while(article_file != NULL)
	{
		if(article_file->d_name[0] == '.'){ article_file = readdir(directory); continue; }
		struct stat stbuf;
		char path[1024];
		strcpy(path,dir_path);
		strcat(path, "/");
		strcat(path, article_file->d_name);
		if(stat(path,&stbuf) != -1)
		{
			if(stbuf.st_mode & S_IFMT != S_IFDIR && type)
			{ article_file = readdir(directory); continue; }
			if(stbuf.st_mode & S_IFMT == S_IFDIR && type == false)
			{ article_file = readdir(directory); continue; }
			files = realloc(files,(count+1)*sizeof(dir_file));
			files[count].name = article_file->d_name;
			strcpy(files[count].path,path);
			files[count].ctime = stbuf.st_ctime;
			files[count].mtime = stbuf.st_mtime;
			count++;
		}
		else { free(article_file); closedir(directory); return NULL;}
		article_file = readdir(directory);
	}
	free(article_file);
	closedir(directory);
	return files;
}

bool sort_name(char *name, char *name2, int nr)
{
	if(name[nr] != '\000' && name2[nr] == '\000')
		return true;
	if(name[nr] > name2[nr])
		return true;	
	if(name[nr] < name2[nr])
		return false;
	if(name[nr] == name[nr])
		return sort_name(name,name2,nr+1);
}

dir_file *sort_dir(dir_file *files, int sort_type)
{
	bool invert = sort_type % 2 ? true : false;
	bool triggered = false;
	int count = 1;
	//remove the invert bit
	sort_type = sort_type >> 1 << 1;
	while(!triggered)
	{
		while(files[count].ctime != 0)
		{
			switch(sort_type)
			{
				case SORT_MTIME:
					if(files[count-1].mtime > files[count].mtime)
						swap(files[count-1],files[count],triggered);
					break;
				case SORT_CTIME:
					if(files[count-1].ctime > files[count].ctime)
						swap(files[count-1],files[count],triggered);
					break;
				case SORT_NAME:
					if(sort_name(files[count-1].name,files[count].name,0))
						swap(files[count-1],files[count],triggered);
					break;
				case SORT_TITLE:
					break;
			}
			count++;
		}
		triggered = triggered ? false : true;
		if(!triggered)
			count = 1;
	}
	if(invert)
	{
		int len = count-1;
		count = 0;
		while(count != len)
		{
			dir_file temp = files[len];
			files[len] = files[count];
			files[count] = temp;
			if(count != (len-1)) { count++; len--; }
			else 
				count = len;	
		}
	}
	return files;
}

