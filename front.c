#include <stdio.h>
#include <time.h>
#include "dir.h"

#define DATE_SEPARATOR "\"--------------%d-%d-%d\n"

//TODO: add limit option
void display_articles_list(char *path, int sort_type, bool date_sep)
{
	dir_file *files = list_items(path,LIST_FILES);
	if(files == NULL)
	{
		puts(".3No articles found!	/");
		return;
	}
	if(files[0].ctime == 0 || files[0].name == NULL)
	{
		puts(".3No articles found!	/");
		free(files);
		return;
	}
	files = sort_dir(files,sort_type);
	s_date cur_date = get_sdate(files[0].ctime);
	if(date_sep)
		printf(DATE_SEPARATOR,cur_date.day,cur_date.month,cur_date.year);
	int count = 0;
	while(files[count].ctime != 0)
	{
		if(date_sep)
		{
			s_date next_date = get_sdate(files[count].ctime);
			if(cur_date.day != next_date.day)
			{
				cur_date = next_date;
				printf(DATE_SEPARATOR,cur_date.day,cur_date.month,cur_date.year);
			}
		}
		char *title = get_article_title(files[count].path);
		printf(".1%s	/%s\n",title,files[count].path);
		count++;
	}
	free(files);
	return;
}

void display_categories_list()
{
	dir_file *dirs = list_items("Categories",LIST_DIRS);
	if(dirs == NULL || dirs[0].ctime == 0)
	{
		puts(".3No categories found!	/\n");
		if(dirs == NULL)
			free(dirs);
		return;
	}
	dirs = sort_dir(dirs,SORT_NAME);
	int count = 0;
	while(dirs[count].ctime != 0)
	{	
		printf(".1%s	%s\n",dirs[count].name,dirs[count].path);
		count++;
	}
	free(dirs);
	return;
}

void main()
{
	char *selector = getenv("QUERY");
	update_selectors();
	if(selector == NULL)
	{
		//main page content
		printf("\"Latest articles:\n");
		display_articles_list("Articles",SORT_CTIME|SORT_REVERSE,false);
	}
	else
	{
		printf("\"Selector: %s\n", selector);
		if(!strcmp(selector,"articles"))
			display_articles_list("Articles", SORT_MTIME|SORT_REVERSE, true);
		if(!strcmp(selector,"categories"))
		{
			printf("\"Categories:\n");
			display_categories_list();
		}
		if(!strncmp(selector,"Categories/",9))
			display_articles_list(selector, SORT_MTIME|SORT_REVERSE, true);
		if(!strncmp(selector,"Test/",9))
			display_articles_list(selector, SORT_MTIME|SORT_REVERSE, true);
	}
	return;
}
