# Gopher News

This is a backend for a dynamic news gopherhole, built using C and requires the [mgod gopher server](https://port70.net/?1mgod).

All articles are placed in the "Articles" folder and symlinked to their respective categories in the "Categories" folder. The backend then creates menus for all paths, automatically sorting the articles and replacing their titles.
